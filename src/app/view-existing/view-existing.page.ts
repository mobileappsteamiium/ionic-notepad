import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Todo, TodoService } from '../services/todo.service';

@Component({
  selector: 'app-view-existing',
  templateUrl: './view-existing.page.html',
  styleUrls: ['./view-existing.page.scss'],
})

export class ViewExistingPage implements OnInit {

  todos: Todo[];
 
  constructor(private todoService: TodoService) { }
 
  ngOnInit() {
    this.todoService.getTodos().subscribe(res => {
      this.todos = res;
    });
  }

  async display()
  {
    console.log(this.todos)
  }
}
