import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewExistingPage } from './view-existing.page';

describe('ViewExistingPage', () => {
  let component: ViewExistingPage;
  let fixture: ComponentFixture<ViewExistingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewExistingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExistingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
