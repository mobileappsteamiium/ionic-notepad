import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Todo, TodoService } from '../services/todo.service';


@Component({
  selector: 'app-new-table',
  templateUrl: './new-table.page.html',
  styleUrls: ['./new-table.page.scss'],
})
export class NewTablePage implements OnInit {

  todos: Todo[];
 
  constructor(private todoService: TodoService) { }
 
  ngOnInit() {
    this.todoService.getTodos().subscribe(res => {
      this.todos = res;
    });
  }
 
  remove(item) {
    this.todoService.removeTodo(item.id);
  }
}
